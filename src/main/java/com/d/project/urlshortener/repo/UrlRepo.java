package com.d.project.urlshortener.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.d.project.urlshortener.entity.UrlEntry;


@Repository
public interface UrlRepo  extends JpaRepository<UrlEntry, Long> {


}
