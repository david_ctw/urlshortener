package com.d.project.urlshortener.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LongUrlRequest {
    @ApiModelProperty(required = true, notes = "URL to be shorten")
    private String longUrl;

    @ApiModelProperty(notes = "Validity expiration datetime")
    private Date expiresDate;
}
