package com.d.project.urlshortener.service;

import org.springframework.stereotype.Service;

@Service
public class BaseConversionService {

	private static final String ALLOWED_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private char[] allowedCharacters = ALLOWED_STRING.toCharArray();
	private int base = allowedCharacters.length;

	/**
	 * this is the encoding function
	 * 
	 * @param input
	 * @return
	 */
	public String encode(long input) {
		StringBuilder encodedString = new StringBuilder();

		if (input == 0) {
			return String.valueOf(allowedCharacters[0]);
		}

		while (input > 0) {
			encodedString.append(allowedCharacters[(int) (input % base)]);
			input = input / base;
		}
		return encodedString.reverse().toString();
	}

	/**
	 * this is the base decoding function
	 * 
	 * @param input
	 * @return
	 */
	public long decode(String input) {
		char[] characters = input.toCharArray();
		int length = characters.length;

		int decoded = 0;

		// counter is used to avoid reversing input string
		int counter = 1;
		for (int i = 0; i < length; i++) {
			decoded += ALLOWED_STRING.indexOf(characters[i]) * Math.pow(base, length - counter);
			counter++;
		}
		return decoded;
	}

}
