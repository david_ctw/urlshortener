package com.d.project.urlshortener.service;

import java.util.Date;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;

import com.d.project.urlshortener.dto.LongUrlRequest;
import com.d.project.urlshortener.entity.UrlEntry;
import com.d.project.urlshortener.repo.UrlRepo;

import lombok.var;

public class UrlEntryService {

	@Autowired
	private UrlRepo urlRepo;

	@Autowired
	private BaseConversionService baseConversionService;

	public String convertToShortUrl(LongUrlRequest request) {
		UrlEntry url = new UrlEntry();
		url.setLongUrl(request.getLongUrl());
		url.setExpiresDate(request.getExpiresDate());
		url.setCreatedDate(new Date());
		UrlEntry entity = urlRepo.save(url);

		return baseConversionService.encode(entity.getId());
	}

	public String getOriginalUrl(String shortUrl) {
		var id = baseConversionService.decode(shortUrl);
		var entity = urlRepo.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("There is no entity with " + shortUrl));

		if (entity.getExpiresDate() != null && entity.getExpiresDate().before(new Date())) {
			urlRepo.delete(entity);
			throw new EntityNotFoundException("Link expired!");
		}

		return entity.getLongUrl();
	}
}
